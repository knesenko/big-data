package org.fp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.codehaus.jackson.map.util.ArrayBuilders;

public class StocksReducer extends Reducer<Text,Vector,Stock,NullWritable> {
	private int numDays;
	private NullWritable nullWriter = NullWritable.get();
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		Configuration conf = context.getConfiguration();
		numDays = conf.getInt("param.days", 0);
	}

	@Override
	public void reduce(Text key, Iterable<Vector> values, Context context)
			throws IOException, InterruptedException {
		
		double[][] stock_full_vector = new double[1][1];

		// check if we got the exact days we need
		int checksum = numDays;
		int features = 0;
		for(Vector value : values) {
			if(checksum==numDays) {
				// first iteration
				// Examine a vector in order to know how many features we have
				features = value.getVector().length-1;
				
				// Create the right array size
				stock_full_vector = new double[features][numDays];
				// Initialize array
				for(int i=0;i<stock_full_vector.length;i++) {
					for(int j=0;j<stock_full_vector[i].length;j++) {
						stock_full_vector[i][j] =  0.0;
					}
				}
			}
			
			checksum--;
			double[] v = value.getVector();
			int day_of_vector = (int) v[0];
			// run over the features values
			for(int i = 1;i<v.length;i++) {
				stock_full_vector[i-1][day_of_vector-1] = v[i];
			}
		}
		
		if(checksum!=0) { return; } // we got less vectors days then we ask for - ABORT!
		
		
		double[] final_vector = new double[features*numDays];
		int index = 0;
		for(int i=0;i<stock_full_vector.length;i++) {
			for(int j=0;j<stock_full_vector[i].length;j++) {
				final_vector[index] = stock_full_vector[i][j];
				index++;
			}
		}
		
		context.write(new Stock(key,final_vector).normalize(),nullWriter);
	}

}
