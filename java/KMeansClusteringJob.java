package org.fp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;


/**
 * This KMeans implementation requires few folders to be ready as following:
 * inputFolder - SequenceFile with <Stock,Stock> . value as data. key as nearest center; key default should be empty
 * centersFolder - SequenceFile with <Stock,Int> for all centers (statring centers).
 * outputFolder - The folder the alg keep the new points
 * @author cloudera
 *
 */
public class KMeansClusteringJob {
	
	//private static final Log LOG = LogFactory.getLog(KMeansClusteringJob.class);

	@SuppressWarnings("deprecation")
	public static int main(int k,String inputFolder,String outputFolder,String centroidsFolder) 
			throws IOException, InterruptedException, ClassNotFoundException {
		// centroidsFolder - is the folder name of the centroids under the main folder

		Configuration conf = new Configuration();
		long counter = Long.MAX_VALUE;
		int iteration = 1;
		FileSystem fs = FileSystem.get(conf);
		Path in = new Path(inputFolder);
		Path out;// = new Path(outputFolder+"/"+centroidsFolder+"/output_"+iteration);
		
		
		// first make the centroids for the centroids folder and choose k
		//if(k<2) { return 0; }
		// generate the folder name of the center
		Path centroids = new Path(outputFolder+"_centroids/"+centroidsFolder);
		// delete folder if exists
		if(fs.exists(centroids)) { fs.delete(centroids, true); }
		// create the folder
		fs.mkdirs(centroids);

		
		// lets choose the starting centroids
		// read the K first stocks from input folder and make them as centroids
		List<Stock> centroidsStocks = new ArrayList<Stock>();
		FileStatus[] fss = fs.listStatus(in);
		for(FileStatus status : fss) {
			if(centroidsStocks.size()==k) { break; }
			Path path = status.getPath();
			if(fs.isDirectory(path)) { continue; }
			SequenceFile.Reader reader = new SequenceFile.Reader(fs, path, conf);
			Stock key = new Stock();
			Stock value = new Stock();
			while (reader.next(key, value)) {
				if(centroidsStocks.size()==k) { break; }
				centroidsStocks.add(new Stock(value));
			}
			reader.close();
		}
		
		// here we should have a list of centroids stocks at size K
		// lets write them to centroids folder
		
		Path centroids_file = new Path(centroids.toString()+"/data");
		fs.createNewFile(centroids_file);
		final SequenceFile.Writer centerWriter = SequenceFile.createWriter(
				conf,
				Writer.file(centroids_file),
				Writer.keyClass(Stock.class),
				Writer.valueClass(IntWritable.class)
				);
		final IntWritable value = new IntWritable(0);
		for(Stock s : centroidsStocks) {
			centerWriter.append(new Stock(s), value);
		}
		centerWriter.close();
		conf.set("KMeans.centroid.path", centroids_file.toString());
		System.out.println("Writing "+centroidsStocks.size()+" starting centers to KMeans..");
		
		while (counter > 0) {

			conf.set("KMeans.num.iteration", iteration + "");
			Job job = new Job(conf);
			job.setJobName("KMeans Clustering " + iteration);

			job.setMapperClass(KMeansMapper.class);
			job.setReducerClass(KMeansReducer.class);
			job.setJarByClass(KMeansMapper.class);
			
			if(iteration!=1) {
				//in = new Path(outputFolder+"_" + (iteration - 1) + "/");
				in = new Path(outputFolder+"/"+centroidsFolder+"/output_"+(iteration-1)+"/");
			}
			out = new Path(outputFolder+"/"+centroidsFolder+"/output_"+iteration);
			
			SequenceFileInputFormat.addInputPath(job, in);
			if (fs.exists(out)) { fs.delete(out, true); }

			SequenceFileOutputFormat.setOutputPath(job, out);
			job.setInputFormatClass(SequenceFileInputFormat.class);
			job.setOutputFormatClass(SequenceFileOutputFormat.class);
			job.setOutputKeyClass(Stock.class);
			job.setOutputValueClass(Stock.class);
			
			job.setNumReduceTasks(1);
			
			job.waitForCompletion(true);
			iteration++;
			counter = job.getCounters()
					.findCounter(KMeansReducer.Counter.CONVERGED).getValue();
		}

		return iteration-1;

	}

}
