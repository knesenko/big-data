package org.fp;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class FinalOutputReducer extends Reducer<Stock,Text,Text,Text> {
	
	@Override
	public void reduce(Stock key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		
		System.out.println("The stock name "+key.getStockName());
		Text center = new Text(key.getStockName());
		
		for(Text value : values) {
			context.write(center, new Text(value));
		}
	}
}