package org.fp;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;

public class Stock extends Vector {
	private Text name;
	
	public Stock() {
		super();
		name = null;
	}
	
	public Stock(Stock s) {
		super(s);
		this.name = new Text(s.name);
	}
	
	public Stock(Text name, double[] vector) {
		super(vector);
		this.name = new Text(name);
	}
	
	public Stock setName(Text name) {
		this.name = new Text(name);
		return this;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		name = new Text();
		name.readFields(in);
		
		super.readFields(in);
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		name.write(out);
		super.write(out);
	}

	
	public boolean converged(Stock o) {
		return compareTo(o) == 0 ? false : true;
	}
	
	public String getStockName() { return name.toString(); }
	
	public Stock normalize() {
		double[] vector = this.getVector();
		double maxValue = vector[0];
		double minValue = vector[0];
		double value;
		
		// run over the vector in order to find max/min value
		for(int i=1;i<vector.length; i++) {
			value = vector[i];
			if(value > maxValue) { maxValue = value; } 
			if(value < minValue) { minValue = value; }
		}
		
		// normalize min-max +1 (values between 1-2)
		for(int i=0;i<vector.length; i++) {
			if(maxValue != minValue) {
				vector[i] = ((vector[i] - minValue) / (maxValue-minValue))+1;
			} else {
				vector[i] = 1;
			}
		}

		this.setVector(vector);
		return this;
	}
}
