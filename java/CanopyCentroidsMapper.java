package org.fp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class CanopyCentroidsMapper extends Mapper<Stock, NullWritable, Text, Stock> {
	private List<Stock> CanopyCenters;
	private double T1;
	private double T2;
	private Text CENTROID;
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		this.CanopyCenters = new ArrayList<Stock>();
		this.T1 = context.getConfiguration().getDouble("canopy.t1", 0.0);
		this.T2 = context.getConfiguration().getDouble("canopy.t2", 0.0);
		this.CENTROID = new Text("centroid");
	}
	
	@Override
	public void map(Stock key, NullWritable value,Context context) throws IOException, InterruptedException {
		for(Stock center : CanopyCenters) {
			System.out.println("Distance between "+key.getStockName()+" to "+center.getStockName()+" is "+DistanceMeasurer.measureDistance(center, key));
			if(DistanceMeasurer.measureDistance(center, key) < T2) { return; }
//			if(DistanceMeasurer.measureDistance(center, key) < T1) {
//				// should add it to center
//				return;
//			}
		}
		Stock newCenter = new Stock(key);
		CanopyCenters.add(newCenter);
		context.write(CENTROID, newCenter);
	}
			
}
