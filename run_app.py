#!/usr/bin/python

import ssh
import utils

import argparse
import logging
import gettext
import sys
import urllib
import os
import fnmatch
import datetime
import time
import shutil

APP_NAME = 'run_app'
NASQAD_URL = 'ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt'
BASE_STOCK_URL = 'http://ichart.yahoo.com/table.csv?s=' \
                 '{company}&a={month}&b={day}&c={year}'

MAX_DAYS = 10
ACTUAL_NUM_DAYS = 0
COMPANIES_NAMES = set()

_ = lambda m: gettext.dgettext(message=m, domain='big-data')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Create a file handler
handler = logging.FileHandler('{name}.log'.format(
    name=APP_NAME,
))

# Create a logging format
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
handler.setFormatter(formatter)

# Add the handlers to the logger
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
logger.info(
    _('Starting new run at {current_time}'.format(
        current_time=time.strftime("%d%m%Y%I%M%S"),
    ))
)


def get_args():
    """
    Handle program parameters
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-directory',
                        help='Input directory from where to take data to copy')
    parser.add_argument('-a', '--application-directory',
                        help='Full path to the application directory from '
                             'where to take java files'
                             'the remote machine')
    parser.add_argument('-n', '--number-stocks', default=100,
                        help='Number of stocks to analyze')
    parser.add_argument('-d', '--days', default=10,
                        help='Number of days to analyze')
    parser.add_argument('-f', '--feature',
                        help='Features to analyze: [open,close,high,low]')
    parser.add_argument('-c', '--clusters',
                        help='Number of clusters')
    parser.add_argument('--cached',
                        help='Use cached directory with existing scv files')
    parser.add_argument('login_server')

    return parser.parse_args()


def download_stock(dir):
    """
    Downloads stock list to the given directory
    Parameters:
        dir - directory where to download the stock file
    """
    if os.path.exists(dir):
        logger.error(
            _('Directory {dir} already exists removing'.format(
                dir=dir,
            ))
        )
        shutil.rmtree(dir)

    logger.info(
        _('Creating the directory {dir}'.format(dir=dir))
    )
    os.makedirs(dir)

    logger.info(
        _('Downloading {src} to {dst}'.format(src=NASQAD_URL,
                                              dst=dir + '/' +
                                              os.path.basename(NASQAD_URL)
                                              ))
    )

    urllib.urlretrieve(NASQAD_URL, dir + '/'
                       + os.path.basename(NASQAD_URL))


def find(pattern, path):
    """
    Searches for a file in the given directory
    Parameters:
        pattern - pattern you want to search for
        path - path in where you want to search

    Returns:
        result - full path to the matched file
    """
    if not os.path.exists(path):
        logger.error(
            _('Directory {dir} does not exists'.format(
                dir=path,
            ))
        )
        sys.exit(utils.ExitCodes.MISSING_DIRECTORY)

    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    logger.info(
        _('Found file {file}'.format(
            file=result,
        ))
    )
    return ''.join(result)


def cut_stock_file(directory, number):
    """
    Cuts the downloaded file according to the number
    Parameters:
        directory - destination directory where all files will land
        number = number of stocks you want to work on
    """
    number = int(number)
    _saved_path = os.getcwd()

    os.chdir(directory)

    _filename = find('*txt', directory)
    # Read the file and divide it into a pieces

    with open(_filename, 'r') as f:
        head = [f.next() for x in xrange(number)]

    for line in head:
        COMPANIES_NAMES.add(line.split('|')[0])

    _tmp_name = _filename + '.test'
    with open(_tmp_name, 'w') as f:
        f.writelines(head)

    logger.debug(
        _('Renaming {src} to {dst}'.format(
            src=_tmp_name,
            dst=_filename,
        ))
    )
    os.rename(_tmp_name, _filename)

    os.chdir(_saved_path)


def download_specific_stocks(directory, num_days, feature):
    """
    Downloads stock information per company profile
    Parameters:
        directory - destination directory where all files will land
        num_days - number of days you want to work on
        feature - provided feature form the command line [open,high,close,low]
    """
    global ACTUAL_NUM_DAYS

    # parse features
    feature = feature.lower().split(',')

    num_days = int(num_days)

    _calculated_date = \
        datetime.date.today() - datetime.timedelta(days=num_days)
    logger.info(
        _('Calculating the date. Calculated date is {date}'.format(
            date=_calculated_date,
        ))
    )

    for company in COMPANIES_NAMES:
        _dst_file = directory + '/' + os.path.basename(company) + '.csv'
        logger.info(
            _('Constructing the download url with dates')
        )

        _down_url = BASE_STOCK_URL.format(
            company=company,
            month=_calculated_date.month - 1,
            day=_calculated_date.day,
            year=_calculated_date.year,
        )

        logger.info(
            _('Downloading {src} to {dst}'.format(src=_down_url,
                                                  dst=_dst_file,
                                                  ))
        )
        urllib.urlretrieve(_down_url, _dst_file)

        # hack lets check if we got a broken stock file
        with open(_dst_file, 'r') as f:
            _lines = f.readlines()

        for line in _lines:
            if 'HTML' in line:
                logger.warning(
                    _('Found corrupted file {file}'.format(
                        file=_dst_file,
                    ))
                )
                logger.info(
                    _('Removing corrupted {file}'.format(
                        file=_dst_file,
                    ))
                )
                os.remove(_dst_file)
                break

        if not os.path.exists(_dst_file):
            logger.info(
                _('{file} was corrupted skipping)'.format(
                    file=_dst_file,
                ))
            )
            continue
        # end of hack

        # cut the file according to a feature

        _file_name = os.path.basename(_dst_file).split('.')[0]
        with open(_dst_file, 'r') as f:
            _lines = f.readlines()

        logger.debug(
            _('Changing the file content according the provided feature')
        )

        _open = _high = _low = _close = ''
        _counter = 1
        _feature_match = False
        with open(_dst_file, 'w') as f:
            for current_line in _lines:
                if 'Date' in current_line:
                    continue

                if 'open' in feature:
                    _open = current_line.split(',')[1] + ','
                    _feature_match = True
                if 'high' in feature:
                    _high = current_line.split(',')[2] + ','
                    _feature_match = True
                if 'low' in feature:
                    _low = current_line.split(',')[3] + ','
                    _feature_match = True
                if 'close' in feature:
                    _close = current_line.split(',')[4]
                    _feature_match = True

                if not _feature_match:
                    logger.error(
                        _('feature "{f}" is not supported'.format(
                            f=feature,
                        ))
                    )
                    sys.exit(utils.ExitCodes.FEATURE_ERROR)

                logger.info(
                    _('Detected "{f}" feature'.format(
                        f=feature,
                    ))
                )
                logger.debug(
                    _('Before the change: {line}'.format(
                        line=current_line,
                    ))
                )
                _line = _file_name + ',' + str(_counter) + ',' \
                    + _open \
                    + _high \
                    + _low \
                    + _close \
                    + '\n'
                _line = _line.strip().rstrip(',')
                _line += '\n'
                logger.debug(
                    _('After the change: {line}'.format(
                        line=_line,
                    ))
                )
                logger.debug(
                    _('Writing to {file}'.format(file=f))
                )
                f.write(_line)
                _counter += 1

        ACTUAL_NUM_DAYS = max(ACTUAL_NUM_DAYS, _counter-1)
        # end cut file

        if num_days > MAX_DAYS:
            logger.info(
                _('Going to split the file into small files. Each file'
                  '{num} lines length'.format(num=MAX_DAYS))
            )
            _part_dst_file = directory + '/' + _file_name + '{part}' + '.csv'

            with open(_dst_file, 'r') as f:
                _lines = f.readlines()

            _file_id = 0
            while _file_id < len(_lines)/float(MAX_DAYS):
                with open(str(_part_dst_file).format(part=_file_id), 'w') \
                        as current_file:
                    for current_line in _lines[MAX_DAYS * _file_id:MAX_DAYS * (
                            _file_id + 1)]:
                        logger.debug(
                            _('Writing to {file}'.format(file=current_file))
                        )
                        current_file.write(current_line)
                    _file_id += 1

            logger.debug(
                _('Removing {file}'.format(
                    file=_dst_file,
                ))
            )
            os.remove(_dst_file)

    _filename = find('*txt', directory)
    os.remove(_filename)


def main():
    global ACTUAL_NUM_DAYS
    args = get_args()

    if '@' not in args.login_server:
        logger.error(
            _('Cannot parse {v}'.format(
                v=args.login_server,
            ))
        )
        sys.exit(utils.ExitCodes.PARSING_ERROR)

    _login, _server = args.login_server.split('@')

    client = ssh.SSHClient(_login, _server, None)
    client.connect()

    if args.input_directory \
            and args.application_directory \
            and args.number_stocks \
            and args.feature \
            and args.clusters:
        download_stock(args.input_directory)
        cut_stock_file(args.input_directory, args.number_stocks)
        download_specific_stocks(args.input_directory, args.days, args.feature)
        logger.info(
            _('Running hadoop with the following parameters:\n'
              'input: {input}\n'
              'java directory: {app_directory}\n'
              'days: {days}\n'
              'features: {features}\n'
              'clusters: {clusters}'.format(input=args.input_directory,
                                            app_directory=args.application_directory,
                                            days=ACTUAL_NUM_DAYS,
                                            features=args.feature,
                                            clusters=args.clusters))
        )
        client.put_files_hdfs(args.input_directory,
                              args.application_directory,
                              ACTUAL_NUM_DAYS,
                              args.feature,
                              args.clusters)
    else:
        logger.error(
            _('Missing one of the options. Exiting')
        )
        sys.exit(utils.ExitCodes.OPTIONS_ARGS_ERROR)

if __name__ == '__main__':
    main()
