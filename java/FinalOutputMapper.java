package org.fp;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class FinalOutputMapper extends Mapper<Stock, Stock, Stock, Text> {

	private int numDays;
	private int numFeatures;
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		Configuration conf = context.getConfiguration();
		numDays = conf.getInt("param.days", 0);
		numFeatures = conf.getInt("param.features", 0);
	}
	
	@Override
	public void map(Stock key, Stock value,Context context) throws IOException, InterruptedException {

		StringBuilder sb = new StringBuilder();
		sb.append(value.getStockName());
		sb.append("|");
		//sb.append(value.printVector());
		
		double[] result = value.getVector();
		double[] final_result = new double[numDays];
		for(int i=0; i<final_result.length;i++) {
			final_result[i] = 0;
			for(int j=0;j<numFeatures; j++) {
				final_result[i] += result[i+(j*numDays)];
			}
		}
		sb.append(Arrays.toString(final_result));
		
		Text stockAsString = new Text(sb.toString());
		context.write(new Stock(key), stockAsString);
	}
		

}
