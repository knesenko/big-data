#!/usr/bin/python
import os
import sys
import shutil as s

CURRENT_CLUSTER = ''
COUNT_CLUSTERS = 0


def openCenterDiv(id):
    out = "\n\t<td>" \
          "\n\t\t<div class='cluster'>" \
          "\n\t\t\t<h2>Cluster #{n}</h1>".format(n=id,)
    return out


def closeCenterDiv():
    out = "\n\t\t</div>" \
          "\n\t</td>"
    return out


def stockDiv(stock, values):
    out = "\n\t<div class='stock'>" \
          "\n\t\t{s}" \
          "\n\t\t</br>" \
          "\n\t\t<span class='inlinesparkline'>".format(s=stock,)
    out += "{val}".format(val=values)
    out += "</span>\n\t</div>"
    return out


def parseFile(file):
    global COUNT_CLUSTERS
    global CURRENT_CLUSTER

    CURRENT_CLUSTER = ''
    out = ''

    current_cluster_stocks = None

    with open(file, 'r') as f:
        lines = f.read().splitlines()

    for line in lines:
        line = line.replace('\t', '|')
        (center, stock, values) = line.split('|')

        "clean values vector to be without clousers"
        values = values.replace('[', '').replace(']', '')
        if center != CURRENT_CLUSTER:
            current_cluster_stocks = list()
            if COUNT_CLUSTERS > 0:
                closeCenterDiv()
            COUNT_CLUSTERS += 1
            CURRENT_CLUSTER = center
            out += openCenterDiv(COUNT_CLUSTERS)
        if stock not in current_cluster_stocks:
            out += stockDiv(stock, values)
            current_cluster_stocks.append(stock)
    out += closeCenterDiv()
    return out


def parseAll(dir):
    out = "\n<table>" \
          "\n\t<tr>"
    for root, _, files in os.walk(dir):
        for f in files:
            fullpath = os.path.join(root, f)
            if f.startswith('part-r'):
                out += parseFile(fullpath)

    out += "\n\t</tr>" \
           "\n</table>"
    return out

if __name__ == '__main__':
    html_file_name = sys.argv[1]
    s.copyfile("templates/layout.htm", "templates/" + html_file_name)
    with open("templates/" + html_file_name, "a") as html:
        html.write('<center><img src="../static/logo_hadoop.jpg"></center><br>')
        html.write('<h1>Hadoop Clustering Results</h1>')
        html.write(parseAll('output'))
        html.write('</body></html>')

    print 'done writing html'
