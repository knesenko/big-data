#!/usr/bin/python

import utils
import scp

import paramiko
import argparse
import gettext
import sys
import uuid
import os
import shutil
import time
import tarfile


_ = lambda m: gettext.dgettext(message=m, domain='big-data')

paramiko.util.log_to_file('ssh.log')


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port',
                        help='Port for ssh', default=2222)
    parser.add_argument('-l', '--login',
                        help='Username you want to login with')
    parser.add_argument('server')
    parser.add_argument('-c', '--command',
                        help='Run command on the remote server')

    return parser.parse_args()


class SSHClient():
    def __init__(self, username, server, password):
        self.server = server
        self.username = username
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.password = password

    def connect(self):
        """
        This method opens connection to the server
        """

        _username = self.username
        _server = self.server
        if '@' in self.server:
            _username, _server = self.server.split('@')

        try:
            self.ssh.connect(_server,
                             username=_username,
                             password=self.password,
                             port=2222)
        except paramiko.AuthenticationException:
            print(
                _('ERROR: Password is wrong for user "{user}"'.format(
                    user=_username,
                ))
            )
            sys.exit(utils.ExitCodes.WRONG_PASSWORD)

    def do_command(self, command):
        """
        This method executes the requested command
        Parameters:
            command - command to execute
        Returns:
            exit status of the executed command
        """
        try:
            chan = self.ssh.get_transport().open_session()
            _stdout = chan.makefile('rb')
            _stderr = chan.makefile_stderr('rb')

            chan.exec_command(command)

            _result = {'stdout': [], 'stderr': []}
            _exit_status = chan.recv_exit_status()
            _result['exit_status'] = _exit_status

            if _result['exit_status'] != 0:
                for line in _stderr:
                    _result['stderr'].append(line)
                    print(
                        _(line.rstrip())
                    )

                print(
                    _('ERROR: Failed to execute "{cmd}" on "{server}"'.format(
                        cmd=command,
                        server=self.server,
                    ))
                )
                print(
                    _('Exit status is: "{exit_status}"'.format(
                        exit_status=_result['exit_status'],
                    ))
                )
                return _result['exit_status']

            print(
                _('INFO: "{cmd}" execution was successful'.format(
                    cmd=command,
                ))
            )
            print(
                _('INFO: Finished with exit status: "{exit_status}"'.format(
                    exit_status=_result['exit_status'],
                ))
            )

            for line in _stdout:
                _result['stdout'].append(line)
                print(
                    _(line.rstrip())
                )

            return _result['exit_status']

        except paramiko.SSHException:
            print(
                _('ERROR: Failed to execute "{cmd}"'.format(
                    cmd=command,
                ))
            )
            sys.exit(utils.ExitCodes.ERROR_EXECUTE_COMMAND)

    def put_files_hdfs(self,
                       src_directory,
                       java_files_dir,
                       number_of_days,
                       feature,
                       num_of_clusters):
        """
        This function will put all from from src_directory to dst_directory
        Parameters:
        src_directory - local source directory
        java_files_dir - application name path to java files
        """
        num_of_features = len(feature.split(','))
        app_name = 'FinalProject'

        if not os.path.exists(src_directory):
            print(
                _('ERROR: {dir} does not exists'.format(
                    dir=src_directory,
                ))
            )
            sys.exit(utils.ExitCodes.ERROR_EXECUTE_COMMAND)

        if not os.path.exists(java_files_dir.split(',')[0]):
            print(
                _('ERROR: {java_files_dir} does not exists'.format(
                    java_path=java_files_dir,
                ))
            )
            sys.exit(utils.ExitCodes.ERROR_EXECUTE_COMMAND)

        _dir_name = '/tmp/{d}'.format(
            d=uuid.uuid4(),
        )

        _cmd = 'mkdir -p {dir}'.format(
            dir=_dir_name,
        )

        # Create a tmp directory to store files
        print(
            _('INFO: Running "{cmd}"'.format(
                cmd=_cmd,
            ))
        )
        self.do_command(_cmd)

        # Copy java files
        for root, __, files in os.walk(java_files_dir):
            for f in files:
                fullpath = os.path.join(root, f)
                print(
                    _('INFO: Copying java "{src}" to "{dst}"'.format(
                        src=fullpath,
                        dst=src_directory + '/' + os.path.basename(fullpath),
                    ))
                )
                shutil.copyfile(fullpath,
                                src_directory + '/' +
                                os.path.basename(fullpath))

        # Try to copy files from the src to remote dst
        scp.copy_files(self.ssh, src_directory, _dir_name)

        _cmd = """DIR=/user/cloudera/{app}/input
export DIR
hadoop fs -ls $DIR || {{
hadoop fs -mkdir -p $DIR && hadoop fs -chown cloudera $DIR
}}""".format(
               app=app_name,)

        print(
            _('INFO: Running "{cmd}"'.format(
                cmd=_cmd,
            ))
        )
        self.do_command(_cmd)

        # Copy all files to the input directory
        _cmd = """DIR=/user/cloudera/{app}/input
export DIR
rm -rf {dir}/{{USERS,URLS,DATES}}
hadoop fs -rm -r -f $DIR/*
hadoop fs -rm -r -f /user/cloudera/output*
hadoop fs -put {dir}/* $DIR
hadoop fs -rm -f /user/cloudera/{app}/input/*.java \
""".format(
            app=app_name,
            dir=_dir_name,)

        print(
            _('INFO: Running command "{cmd}"'.format(
                cmd=_cmd,
            ))
        )

        self.do_command(_cmd)

        # Compile java app
        _tmp_java_path = _dir_name
        if len(java_files_dir.split(',')) > 1:
            _tmp_java_path = ''
            for i in java_files_dir.split(','):
                _tmp_java_path += _dir_name + '/' + i + ' '

        print _tmp_java_path

        _cmd = """
[[ -d /home/cloudera/{app} ]] || mkdir -p /home/cloudera/{app}
export PATH=/sbin:/usr/java/jdk1.6.0_32/bin:\
/usr/local/apache-ant/apache-ant-1.9.2/bin\
:/usr/local/apache-maven/apache-maven-3.0.4/bin\
:/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin\
:/usr/sbin:/sbin:/home/cloudera/bin

javac -cp "/usr/lib/hadoop/*:/usr/lib/hadoop/client-0.20/*" \
-d /home/cloudera/{app} {java_path}
jar -cvf /home/cloudera/{app}.jar -C /home/cloudera/{app} .
""".format(
            app=app_name,
            java_path=_tmp_java_path + '/*.java',
        )

        print(
            _('INFO: Running command "{cmd}"'.format(
                cmd=_cmd,
            ))
        )

        self.do_command(_cmd)

        # Run app
        # pkg = app_name
        #
        # with open(java_files_dir.split(',')[0], 'r') as f:
        #     lines = f.readlines()
        #     for i in lines:
        #         if re.match(r'^package', i):
        #             pkg = i.split()[-1].split(';')[0] + '.' + app_name

        _cmd = """
hadoop fs -rm -r -f /user/cloudera/{app}/output
hadoop jar /home/cloudera/{app}.jar {pkg} /user/cloudera/{app}/input \
/user/cloudera/{app}/output {num_days} {num_clusters} {num_features} |& tee /tmp/{app}_output
""".format(
            app=app_name,
            pkg='org.fp.MainJob',
            num_days=number_of_days,
            num_clusters=num_of_clusters,
            num_features=num_of_features,
        )

        print(
            _('INFO: Running command "{cmd}"'.format(
                cmd=_cmd,
            ))
        )

        self.do_command(_cmd)

        # Get the answer
        _cmd = """
rm -rf /home/cloudera/output*
hadoop fs -copyToLocal /user/cloudera/{app}/output /home/cloudera
cp /tmp/{app}_output /home/cloudera/{app}_output
tar -czf /home/cloudera/output.tar.gz output {app}_output
""".format(
            app=app_name,
        )

        print(
            _('INFO: Running command "{cmd}"'.format(
                cmd=_cmd,
            ))
        )

        self.do_command(_cmd)

        # Copy files to local machine
        print(
            _('INFO: Going to copy /home/cloudera/output.tar.gz the '
              'result to the local machine')
        )

        sftp = self.ssh.open_sftp()

        _time = time.strftime("%Y%m%d%I%M%S")
        _tar_name = 'output_{current_time}.tar.gz'.format(current_time=_time)
        sftp.get('/home/cloudera/output.tar.gz', _tar_name)

        if os.path.exists('output'):
            print(_('Going to remove {dir}'.format(dir='output')))
            shutil.rmtree('output')
        if os.path.exists('templates/index.htm'):
            print(_('Going to remove {file}'.format(
                file='templates/index.htm')))
            os.remove('templates/index.htm')

        # untar the file
        tar = tarfile.open(_tar_name)
        tar.extractall()

        # run the server for displaying the stocks
        os.system('python output_parser.py index.htm')


def main():
    args = get_args()

    _login = args.login
    if not args.login:
        _login = args.server.split('@')[0]

    try:
        password = utils.ask_password(_login)
    except KeyboardInterrupt:
        print(
            _('\nCanceled by user')
        )
        sys.exit(utils.ExitCodes.OK)

    client = SSHClient(args.login, args.server, password)
    client.connect()

    if args.command:
        client.do_command(args.command)

if __name__ == '__main__':
    main()
