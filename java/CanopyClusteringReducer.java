package org.fp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
/*
 * this reducer will get each centroid and its related dots (stocks) and write it as a seprated file
 * in a sperated folder . reudcer will write the key - folder name and value - count of points
 */
public class CanopyClusteringReducer extends Reducer<Stock,Stock,Text,LongWritable> {
	//private double T2;
	//private NullWritable nullWriter = NullWritable.get();
	private String centersOutputFolder; // this should get the folder name where to save all points related to center
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		this.centersOutputFolder = context.getConfiguration().get("canopy.centers.path");
		

	}

	@SuppressWarnings("deprecation")
	@Override
	public void reduce(Stock key, Iterable<Stock> values, Context context)
			throws IOException, InterruptedException {
		
		Configuration conf = context.getConfiguration();
		FileSystem fs = FileSystem.get(conf);
		
		// Examine the key stock in order to know how long its vector
		int vector_length = key.getVector().length;
		
		// Create the right array size
		double[] stock_empty_vector = new double[vector_length];
		// Initialize array
		for(int i=0;i<stock_empty_vector.length;i++) {
			stock_empty_vector[i] =  0.0;
		}
		
		//empty stock
		Stock empty_stock = new Stock(new Text("empty"),stock_empty_vector);
		
		
		System.out.println("canopy clustering reducer..");
		
		// generate the folder name of the center
		long time = new Date().getTime();
		String folderName = key.getStockName()+"_"+time;
		Path centers = new Path(this.centersOutputFolder+"/"+folderName); // get the path where to keep all points of the center
		
		// delete folder if exists
		if(fs.exists(centers)) { fs.delete(centers, true); }
		// create the folder
		fs.mkdirs(centers);
		System.out.println("Finish creating the folder");
		
		System.out.println("folder centers is :"+centers.toString());
		
		// path of the file under the mentioned folder where all data will be saved
		// data as sequencefile key = empty stock. value = stock in center
		Path centers_file = new Path(centers.toString()+"/file1");
		fs.createNewFile(centers_file);
		final SequenceFile.Writer centerWriter = SequenceFile.createWriter(
				conf,
				Writer.file(centers_file),
				Writer.keyClass(Stock.class),
				Writer.valueClass(Stock.class)
				);
		
		Map<Stock,Integer> stocks_map = new HashMap<Stock, Integer>();
		for(Stock s : values) {
			stocks_map.put(new Stock(s),0);
		}
		
		for(Stock stock : stocks_map.keySet()) {
			System.out.println("write a stock.. "+stock.getStockName());
			centerWriter.append(new Stock(empty_stock), new Stock(stock));
		}
		centerWriter.close();
		
		// finally - save the folder name and number of stocks as <key,value> accordingly
		context.write(new Text(folderName), new LongWritable(stocks_map.size()));
	}
}
