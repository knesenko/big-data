#!/usr/bin/python

import argparse
import gettext
import sys
import re
import utils
import paramiko
import os


_ = lambda m: gettext.dgettext(message=m, domain='big-data')


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('source')
    parser.add_argument('destination')

    return parser.parse_args()


def copy_files(client, src_file, dst_file):
    _is_dir = False
    _ftp = client.open_sftp()

    if os.path.isdir(src_file):
        _is_dir = True

    print(
        _('Going to scp files "{src_file}" to "{dst_file}"'.format(
            src_file=src_file,
            dst_file=dst_file,
        ))
    )
    try:
        if not _is_dir:
            _ftp.get(src_file, dst_file)
            sys.exit(utils.ExitCodes.OK)

        print(
            _('INFO: Detected source directory')
        )
        for root, dir, file in os.walk(src_file):
            if len(file) > 1:
                for i in file:
                    _ftp.put(root + '/' + i,
                             dst_file + '/' + i)
                return

            _ftp.put(root + '/' + ''.join(file),
                     dst_file + '/' + file)

    except IOError as e:
        print(
            _('ERROR: Cannot copy files')
        )
        print e
        sys.exit(utils.ExitCodes.ERROR_EXECUTE_COMMAND)


def main():
    args = get_args()

    if '@' in args.source:
        _username, _server, _filename = re.split('@|:', args.source)
        _src_file = _filename
        _dest_file = args.destination
    elif '@' in args.destination:
        _username, _server, _filename = re.split('@|:', args.destination)
        _src_file = args.source
        _dest_file = _filename
    else:
        print(
            _('ERROR: Please check the command line.'
              '[[user@]host1:]file1 ... [[user@]host2:]file2')
        )
        sys.exit(utils.ExitCodes.ERROR_IN_COMMAND_LINE)

    _password = utils.ask_password(_username)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(_server, username=_username, password=_password)
    except paramiko.AuthenticationException:
        print(
            _('ERROR: Cannot login authenticate with the '
              'following credentials')
        )
        sys.exit(utils.ExitCodes.WRONG_PASSWORD)

    copy_files(client, _src_file, _dest_file)

if __name__ == '__main__':
    main()
