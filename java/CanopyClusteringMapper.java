package org.fp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class CanopyClusteringMapper extends Mapper<Stock, NullWritable, Stock, Stock> {
	private List<Stock> CanopyCenters;
	//private double T1;
	private double T2;
	@SuppressWarnings("deprecation")
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		this.CanopyCenters = new ArrayList<Stock>();
		//this.T1 = context.getConfiguration().getDouble("canopy.t1", 0.0);
		this.T2 = context.getConfiguration().getDouble("canopy.t2", 0.0);
		
		// load the centroids from file
		Configuration conf = context.getConfiguration();
		Path centroids = new Path(conf.get("canopy.centroid.path"));
		FileSystem fs = FileSystem.get(conf);
		FileStatus[] fss = fs.listStatus(centroids);
		System.out.println("Start loading all centers");
		for(FileStatus status : fss) {
			Path path = status.getPath();
			if(path.getName().toLowerCase().contains("success")) { continue; }
			if(fs.isDirectory(path)) { continue; }
			SequenceFile.Reader reader = new SequenceFile.Reader(fs, path, conf);
			Stock key = new Stock();
			NullWritable value = NullWritable.get();
			while (reader.next(key, value)) {
				CanopyCenters.add(new Stock(key));
			}
			reader.close();
		}

		System.out.println("loaded "+CanopyCenters.size()+"canopy stocks");
		for(Stock s:CanopyCenters) {
			System.out.println("center name :"+s.getStockName());
		}
	}
	
	@Override
	public void map(Stock key, NullWritable value,Context context) throws IOException, InterruptedException {
		for(Stock center : CanopyCenters) {
//			if(center.getStockName().equals(key.getStockName())) {
//				continue;
//			}
			// if this point inside the higher radius - please insert it to this center 
			if(DistanceMeasurer.measureDistance(center, key) <= T2) {
				context.write(new Stock(center), new Stock(key));
			}
		}
		
	}
			
}
