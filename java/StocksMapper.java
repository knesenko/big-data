package org.fp;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

public class StocksMapper extends Mapper<Text, Text, Text, Vector> {

	@Override
	public void map(Text key, Text value,Context context) throws IOException, InterruptedException {
		
		String row = key.toString();
		String[] row_vector = row.split(",");
		// row_vector length should be according to following structure:
		// #stock_name,#day,#feature1...#feature4 = min 3 - max 6
		if(row_vector.length>=3 && row_vector.length<=6) {
			Text stock_name = new Text(row_vector[0]);
			
			double[] day = new double[row_vector.length-1];
			for(int i=1; i<row_vector.length; i++) {
				day[i-1] = Double.valueOf(row_vector[i]);
			}
			context.write(stock_name, new Vector(day));
		}
		
	}

}
