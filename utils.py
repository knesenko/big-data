import getpass


class ExitCodes:
    OK = 0
    ERROR_EXECUTE_COMMAND = 1
    WRONG_PASSWORD = 2
    ERROR_IN_COMMAND_LINE = 3
    PARSING_ERROR = 4
    FEATURE_ERROR = 5
    OPTIONS_ARGS_ERROR = 6
    MISSING_DIRECTORY = 7


def ask_password(user):
    return getpass.getpass('Password for {user}: '.format(
        user=user,
    ))
