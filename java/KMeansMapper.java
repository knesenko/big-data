package org.fp;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.mapreduce.Mapper;

// first iteration, k-random centers, in every follow-up iteration we have new calculated centers
public class KMeansMapper extends Mapper<Stock, Stock, Stock, Stock> {

	List<Stock> centers = new LinkedList<Stock>();

	@SuppressWarnings("deprecation")
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		Configuration conf = context.getConfiguration();
		Path centroids = new Path(conf.get("KMeans.centroid.path"));
		FileSystem fs = FileSystem.get(conf);

		SequenceFile.Reader reader = new SequenceFile.Reader(fs, centroids, conf);
		Stock key = new Stock();
		IntWritable value = new IntWritable();
		while (reader.next(key, value)) {
			centers.add(new Stock(key));
		}
		reader.close();
		System.out.println(centroids.toString()+"we got "+centers.size()+" centers for this KMeans");
	}

	@Override
	protected void map(Stock key, Stock value, Context context) 
			throws IOException, InterruptedException {

		Stock nearest = null;
		double nearestDistance = Double.MAX_VALUE;
		for (Stock c : centers) {
			double dist = DistanceMeasurer.measureDistance(c, value);
			if (nearest == null || nearestDistance > dist) {
				nearest = c;
				nearestDistance = dist;
			}
		}
		context.write(nearest, value);
	}

}
