package org.fp;

public class DistanceMeasurer {

/**
* Manhattan stuffz.
* 
* @param Stock
* @param Stock
* @return
*/

	public static final double measureDistance(Stock s1, Stock s2) {
		double sum = 0;
		double[] s1_vector = s1.getVector();
		double[] s2_vector = s2.getVector();
		
		int length = s1_vector.length;
		for (int i = 0; i < length; i++) {
			sum += Math.abs(s1_vector[i] - s2_vector[i]);
		}

		return sum;
	}

}