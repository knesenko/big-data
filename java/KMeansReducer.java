package org.fp;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Time;

// calculate a new stock center for these vertices
public class KMeansReducer extends
		Reducer<Stock, Stock, Stock, Stock> {

	public static enum Counter {
		CONVERGED
	}

	List<Stock> centers = new LinkedList<Stock>();

	@Override
	protected void reduce(Stock key, Iterable<Stock> values, Context context)
				throws IOException, InterruptedException {

		
		List<Stock> stockList = new LinkedList<Stock>();
		int vectorSize = key.getVector().length;
		// Initialize the average vector
		double[] newCenterVector = new double[vectorSize];
		for(int i=0; i < newCenterVector.length; i++) {
			newCenterVector[i] = 0.0;
		}
		
		
		for (Stock value : values) {
			stockList.add(new Stock(value));
			for (int i = 0; i < value.getVector().length; i++) {
				newCenterVector[i] += value.getVector()[i];
			}
		}

		for (int i = 0; i < newCenterVector.length; i++) {
			newCenterVector[i] = newCenterVector[i] / stockList.size();
		}

		Stock newCenter = new Stock(new Text("avg.of."+key.getStockName()+"."+Time.now()),newCenterVector);
		
		centers.add(newCenter);
		for (Stock stock : stockList) {
			context.write(new Stock(newCenter), new Stock(stock));
		}

		if (newCenter.converged(key))
			context.getCounter(Counter.CONVERGED).increment(1);

	}

	@SuppressWarnings("deprecation")
	@Override
	// write the centers to centers folder
	protected void cleanup(Context context) throws IOException, InterruptedException {
		super.cleanup(context);
		Configuration conf = context.getConfiguration();
		Path outPath = new Path(conf.get("KMeans.centroid.path"));
		FileSystem fs = FileSystem.get(conf);
		fs.delete(outPath, true);
		final SequenceFile.Writer out = SequenceFile.createWriter(
				fs,
				context.getConfiguration(),
				outPath,
				Stock.class,
				IntWritable.class);
		final IntWritable value = new IntWritable(0);
		for (Stock center : centers) {
			out.append(center, value);
		}
		out.close();
	}
}
