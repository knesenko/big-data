#!/usr/bin/python
from flask import Flask
from flask import render_template
from flask import request
import os

INPUT_DIR = '/home/knesenko/git_repos/big-data/java'
app = Flask(__name__)

@app.route('/cluster/run', methods=['POST'])
def post():
    features = []
    if request.method == 'POST':
        # stocksNumber = request.form['stocksNumber']
        # daysNumber = request.form['daysNumber']
        # clustersNumber = request.form['clustersNumber']
        params = dict()
        params['isHigh'] = False
        params['isLow'] = False
        params['isClose'] = False
        params['isOpen'] = False
        for var in request.form:
            params[var] = request.form[var]

        if params['isHigh']:
            features.append('high')
        if params['isLow']:
            features.append('low')
        if params['isClose']:
            features.append('close')
        if params['isOpen']:
            features.append('open')

        if len(features) != 1:
            _features = ','.join(features)
        else:
            _features = ''.join(features)

        # should call to script
        cmd = 'python run_app.py cloudera@localhost -a {app_path} ' \
              '-i {input} -n {numbers} -d {days} -f {features} ' \
              '-c {clusters}'.format(
              app_path=INPUT_DIR,
              input='/tmp/test',
              numbers=params['stocksNumber'],
              days=params['daysNumber'],
              features=_features,
              clusters=params['clustersNumber'])
        print('Running {cmd}'.format(
            cmd=cmd,
        ))
        os.system(cmd)
        return '/cluster'
    return False


@app.route('/')
def index():
    return render_template('form.htm', e=None)

@app.route('/cluster', methods=['POST', 'GET'])
def cluster_result():
    error = None
    # if request.method == 'POST':
        # return request.form['username']
    return render_template('index.htm', error=error)

if __name__ == '__main__':
    app.run(debug=True)
