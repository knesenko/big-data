package org.fp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.map.HashedMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MainJob {

	public static Configuration conf = new Configuration();
	
	
	@SuppressWarnings("deprecation")
	// this job take a center folder (after kmeans run) and prints it to csv format
	public static int FinalOutputJob(String inputFolder, String outputFolder, String centerFolder, int iter_folder) 
			throws IOException, InterruptedException, ClassNotFoundException {
		Job job = new Job(conf,"f.output");

	    job.setMapperClass(FinalOutputMapper.class);
	    job.setReducerClass(FinalOutputReducer.class);
	    job.setJarByClass(FinalOutputMapper.class);
	    
	    Path in = new Path(inputFolder+"/"+centerFolder+"/output_"+iter_folder+"/");
	    Path out = new Path(outputFolder+"/"+centerFolder+"/");
	    //FileSystem fs = FileSystem.get(conf);
	    
	    //System.out.println(in.toString() + " is dir? " + fs.isDirectory(in));
	    //System.out.println(outputFolder+"/ is dir? " + fs.isDirectory(new Path(outputFolder+"/")));
	    
	    SequenceFileInputFormat.addInputPath(job, in);
	    TextOutputFormat.setOutputPath(job, out);
	    
	    //configure separator - supports all APIs
    	String separator = "\\|";
        conf.set("mapred.textoutputformat.separator", separator); //Prior to Hadoop 2 (YARN)
        conf.set("mapreduce.textoutputformat.separator", separator);  //Hadoop v2+ (YARN)
        conf.set("mapreduce.output.textoutputformat.separator", separator);
        conf.set("mapreduce.output.key.field.separator", separator);
        conf.set("mapred.textoutputformat.separatorText", separator); // ?
	    
	    
	    job.setInputFormatClass(SequenceFileInputFormat.class); // how mapper will read the files
	    job.setOutputFormatClass(TextOutputFormat.class); // how reducer will write the files
	    
	    job.setMapOutputKeyClass(Stock.class); // this how map will out the key
	    job.setMapOutputValueClass(Text.class); // this how map will out the value
	    job.setOutputKeyClass(Text.class); // this how reducer will out the key
	    job.setOutputValueClass(Text.class); // this how reducer will out the value
	    
	    return job.waitForCompletion(true) ? 0 : 1;
	}
	
	
	@SuppressWarnings("deprecation")
	public static int StockJob(String inputFolder, String outputFolder) throws IOException, InterruptedException, ClassNotFoundException {
		Job job = new Job(conf,"Stocks First Mapper");

	    
	    job.setMapOutputKeyClass(Text.class); // this how map will out the key
	    job.setMapOutputValueClass(Vector.class); // this how map will out the value

	    job.setMapperClass(StocksMapper.class);
	    job.setReducerClass(StocksReducer.class);
	    job.setJarByClass(StocksMapper.class);
	    SequenceFileOutputFormat.setOutputPath(job,new Path(outputFolder));
	    KeyValueTextInputFormat.addInputPath(job, new Path(inputFolder));
	    
	    job.setInputFormatClass(KeyValueTextInputFormat.class); // how mapper will read the files
	    job.setOutputFormatClass(SequenceFileOutputFormat.class); // how reducer will write the files
	    
	    job.setOutputKeyClass(Stock.class); // this how reducer will out the key
	    job.setOutputValueClass(NullWritable.class); // this how reducer will out the value
	    
	    
	    //FileOutputFormat.setOutputPath(job, );

	    return job.waitForCompletion(true) ? 0 : 1;
		
	}
	
	@SuppressWarnings("deprecation")
	public static Map<String, Integer> KMeansJob(String inputFolder,String centersInputFolder, String outputFolder) throws IOException, InterruptedException, ClassNotFoundException {
		// input folder = the folder when the meta-data on the centers is saved.
		// center input folder = the main folder where all the sub-folders of centers are saved.
		// output folder - the prefix name of kmeans output folders
		// read all centers data (key as folder name, value as count)
		int k = conf.getInt("param.k", 3);
		Path in = new Path(inputFolder);
		
		FileSystem fs = FileSystem.get(conf);
		FileStatus[] fss = fs.listStatus(in);
		System.out.println("Start loading all centers");
		Map<String,Long> mapCenters = new HashMap<String,Long>();
		for(FileStatus status : fss) {
			Path path = status.getPath();
			if(path.getName().toLowerCase().contains("success")) { continue; }
			if(fs.isDirectory(path)) { continue; }
		
			SequenceFile.Reader reader = new SequenceFile.Reader(
					FileSystem.get(conf),
					path,
					conf
			);
		
			Text key = new Text();
			LongWritable value = new LongWritable();
			
			
			while(reader.next(key,value)) {
				mapCenters.put(key.toString(), new Long(value.get()));
			}
			reader.close();
		}
		
		
		// Get entries and sort them.
		List<Entry<String, Long>> entries = new ArrayList<Entry<String, Long>>(mapCenters.entrySet());
		Collections.sort(entries, new Comparator<Entry<String, Long>>() {
		    public int compare(Entry<String, Long> e1, Entry<String, Long> e2) {
		        return e2.getValue().compareTo(e1.getValue());
		    }
		});

		int howManyKMeans = Math.min(mapCenters.size(), k);
		
		// Put entries back in an ordered map.
		Map<String, Long> orderedMap = new LinkedHashMap<String, Long>();
		for (Entry<String, Long> entry : entries) {
		    if(howManyKMeans==0) { break; }
			orderedMap.put(entry.getKey(), entry.getValue());
		    howManyKMeans--;
		}
		
		mapCenters.clear();
		mapCenters.putAll(orderedMap);
		
		
		// calculate the K per each cluster
		// count all points from all canopy centers
		long sumPoints = 0;
		for(Long l : orderedMap.values()) {
			sumPoints+=l;
		}
		// ratio between points to k
		long pointsPerK =  sumPoints / k;
		
		// k to share between centers
		int totalK = k;
		
		// run over the centers and decide the k per each one
		
		for(Entry<String,Long> entry : orderedMap.entrySet()) {
			long kPerCenter = Math.round(entry.getValue() / (double)pointsPerK);
			// check if the propose K is ok with the left Ks to share
			if(totalK-kPerCenter>=0) {
				totalK -= kPerCenter;
				mapCenters.put(entry.getKey(),kPerCenter);
			} else { 
				// if not so finish share the K
				break;
			}
		}
		
		// if K is still need to be share
		while(totalK>0) {
			for(Entry<String,Long> entry : orderedMap.entrySet()) {
				long newValue = mapCenters.get(entry.getKey())+1;
				mapCenters.put(entry.getKey(), newValue);
				totalK--;
				if (totalK==0) { break; }
			}
		}
		
		
		Map<String, Integer> centers_iter = new HashMap<String,Integer>();
		for(Entry<String,Long> entry : mapCenters.entrySet()) {
			System.out.println("key as folder name of center is "+entry.getKey() + " K is "+entry.getValue());
			if(entry.getValue()==0) { 
				System.out.println("K is 0 - skip this !");
				continue;
			}
			int itr = KMeansClusteringJob.main(entry.getValue().intValue(),centersInputFolder+"/"+entry.getKey(),outputFolder,entry.getKey());
			centers_iter.put(entry.getKey(), itr);
			System.out.println("KMeans for "+entry.getKey()+" did "+itr+" iterations..");
		}
		
		
		return centers_iter;
	}
	
	@SuppressWarnings("deprecation")
	public static int CanopyJob(String inputFolder, String outputFolder) throws IOException, InterruptedException, ClassNotFoundException {
		Job job = new Job(conf,"Canopy First Step - Finding Centers");

	    
	    job.setMapOutputKeyClass(Text.class); // this how map will out the key
	    job.setMapOutputValueClass(Stock.class); // this how map will out the value

	    job.setMapperClass(CanopyCentroidsMapper.class);
	    job.setReducerClass(CanopyCentroidsReducer.class);
	    job.setJarByClass(CanopyCentroidsMapper.class);
	    SequenceFileInputFormat.addInputPath(job, new Path(inputFolder));
	    String centroidJobOutputFolder = outputFolder+"_1";
	    SequenceFileOutputFormat.setOutputPath(job,new Path(centroidJobOutputFolder));
	    
	    job.setInputFormatClass(SequenceFileInputFormat.class); // how mapper will read the files
	    job.setOutputFormatClass(SequenceFileOutputFormat.class); // how reducer will write the files
	    
	    job.setOutputKeyClass(Stock.class); // this how reducer will out the key
	    job.setOutputValueClass(NullWritable.class); // this how reducer will out the value
	    
	    job.setNumReduceTasks(1); // must 1 in order to get all centers after the mapping

	    job.waitForCompletion(true);
	    
	    
	    // next step - clustering the points
	    conf.set("canopy.centroid.path", centroidJobOutputFolder);
	    // where should the job keep the new data
	    String clusteringCentersJobOutputFolderFormat = outputFolder+"_2_centers";
	    conf.set("canopy.centers.path", clusteringCentersJobOutputFolderFormat);
		Job job2 = new Job(conf,"Canopy Second Step - Relating Centers");

	    
	    job2.setMapOutputKeyClass(Stock.class); // this how map will out the key
	    job2.setMapOutputValueClass(Stock.class); // this how map will out the value

	    job2.setMapperClass(CanopyClusteringMapper.class);
	    job2.setReducerClass(CanopyClusteringReducer.class);
	    job2.setJarByClass(CanopyClusteringMapper.class);

	    SequenceFileInputFormat.addInputPath(job2, new Path(inputFolder)); // should get as input all the points
	    
	    String clusteringJobOutputFolder = outputFolder+"_2";
	    conf.set("canopy.centers.metadata.path", clusteringJobOutputFolder);
	    SequenceFileOutputFormat.setOutputPath(job2,new Path(clusteringJobOutputFolder));
	    
	    job2.setInputFormatClass(SequenceFileInputFormat.class); // how mapper will read the files
	    job2.setOutputFormatClass(SequenceFileOutputFormat.class); // how reducer will write the files
	    
	    job2.setOutputKeyClass(Text.class); // this how reducer will out the key
	    job2.setOutputValueClass(LongWritable.class); // this how reducer will out the value
	    
	    
	    
	    
	    return job2.waitForCompletion(true) ? 0 : 1;
		
	}
	
//	public static void printResults(String where) throws FileNotFoundException, IOException {
//		FileSystem fs = FileSystem.get(conf);
//		FileStatus[] fss = fs.listStatus(new Path(where));
//		for(FileStatus status : fss) {
//			Path path = status.getPath();
//			if(path.getName().toLowerCase().contains("success")) { continue; }
//			@SuppressWarnings("deprecation")
//			SequenceFile.Reader reader = new SequenceFile.Reader(fs,path,conf);
//			
//			Stock value = new Stock();
//			Stock key = new Stock();
//			
//			while(reader.next(key,value)) {
//				System.out.println("key: "+key.getStockName()+" value:"+value.getStockName());
//			}
//			
//			reader.close();
//		}
//	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		
//		FinalOutputJob("output_kmeans","tst_output","AAIT_1388844074415",2);
//		printResults("output_canopy_2_centers/ACOR_1388912178708");
//		Thread.sleep(1000);
		
		if(args.length != 5) { return; }
		// set all needed configurations
		String inputFolder = args[0];
		String finalOutputFolder = args[1];
		String firstStepOutputFolder = "output_1";
		int numOfDays = Integer.valueOf(args[2]);
		int k = Integer.valueOf(args[3]);
		int numOfFeatures = Integer.valueOf(args[4]);
		conf.setInt("param.days", numOfDays);
		conf.setInt("param.features", numOfFeatures);
		conf.setInt("param.k", k);
		conf.setDouble("canopy.t1", 0.18*numOfDays*numOfFeatures);
		conf.setDouble("canopy.t2", 0.30*numOfDays*numOfFeatures);
		// start step 1
		StockJob(inputFolder, firstStepOutputFolder);
		
//		{
//
//			SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.get(conf), new Path(firstStepOutputFolder+"/part-r-00000"), conf);
//			
//			
//			NullWritable value = NullWritable.get();
//			Stock key = new Stock();
//			
//			List<Stock> stocks = new ArrayList<Stock>();
//			while(reader.next(key,value)) {
//				System.out.println("we got it!"+key.getStockName());
//				stocks.add(new Stock(key));
//			}
//			
//			for(Stock s1 : stocks) {
//				for(int i=0; i<stocks.size();i++) {
//					System.out.println("Distance between "+s1.getStockName()+" to "+stocks.get(i).getStockName()+" is "+DistanceMeasurer.measureDistance(s1, stocks.get(i)));
//				}
//			}
//			reader.close();
//		}
		
		// step 2 - canopy clustering
		String secondStepOutputFolder = "output_canopy";
		CanopyJob(firstStepOutputFolder, secondStepOutputFolder);

		// step 3 - KMeans
		String centersMetaDataInputFolder = conf.get("canopy.centers.metadata.path");
		String centersInputMainFolder = conf.get("canopy.centers.path");
		String kmeansPrefixOutputFolder = "output_kmeans";
		Map<String, Integer> kmeans_centers = KMeansJob(centersMetaDataInputFolder,centersInputMainFolder,kmeansPrefixOutputFolder);
		FileSystem fs = FileSystem.get(conf);
		fs.mkdirs(new Path(finalOutputFolder+"/"));
		
		
			for(Entry<String,Integer> entry : kmeans_centers.entrySet()) {
				
				
				FileStatus[] fss = fs.listStatus(new Path(kmeansPrefixOutputFolder+"/"+entry.getKey()+"/output_"+entry.getValue()+"/"));
				for(FileStatus status : fss) {
					Path path = status.getPath();
					if(path.getName().toLowerCase().contains("success")) { continue; }
					if(fs.isDirectory(path)) { continue; }
					SequenceFile.Reader reader = new SequenceFile.Reader(fs,path,conf);
					
					Stock value = new Stock();
					Stock key = new Stock();
					
					while(reader.next(key,value)) {
						System.out.println(path.getName()+" : we got it!"+value.getStockName()+" d="+DistanceMeasurer.measureDistance(key, value));
					}
					
					reader.close();
				}
				
				FinalOutputJob(kmeansPrefixOutputFolder, finalOutputFolder, entry.getKey(),entry.getValue());
				
			}
			

		

	}

}
