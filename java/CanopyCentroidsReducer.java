package org.fp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class CanopyCentroidsReducer extends Reducer<Text,Stock,Stock,NullWritable> {
	private double T2;
	private NullWritable nullWriter = NullWritable.get();
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		this.T2 = context.getConfiguration().getDouble("canopy.t2", 0.0);
	}

	@Override
	public void reduce(Text key, Iterable<Stock> values, Context context)
			throws IOException, InterruptedException {
		
		List<Stock> FinalCanopyCenters = new ArrayList<Stock>();
		boolean toSkip;
		
		for(Stock center: values) {
			toSkip = false;
			for(Stock final_center : FinalCanopyCenters) {
				if(DistanceMeasurer.measureDistance(center, final_center) < T2) { toSkip=true;break; }
			}
			if(!toSkip) { // should add to final centers
				Stock addingCenter = new Stock(center);
				FinalCanopyCenters.add(addingCenter);
			}
		}
		
		for(Stock center: FinalCanopyCenters) {
			context.write(center, nullWriter);
		}
		System.out.println("Canpoy centroids count is : "+FinalCanopyCenters.size());
	}
}
